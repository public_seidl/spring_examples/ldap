package sk.seidl.ldap_stuba.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.seidl.ldap_stuba.mapper.MyMapper;
import sk.seidl.ldap_stuba.models.User;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import java.util.List;

@RestController
public class BaseController {

    private static Logger log = LoggerFactory.getLogger(BaseController.class);

    private LdapTemplate ldapTemplate;


    @Autowired
    public BaseController(LdapTemplate ldapTemplate) {
        this.ldapTemplate = ldapTemplate;
    }

    // localhost:8080/
    @GetMapping("/")
    public String index() {

        log.info("Getting UsernamePasswordAuthenticationToken from SecurityContextHolder");
        UsernamePasswordAuthenticationToken authentication =
                (UsernamePasswordAuthenticationToken)
                        SecurityContextHolder.getContext().getAuthentication();

        log.info("Getting principal from UsernamePasswordAuthenticationToken");
        LdapUserDetailsImpl principal = (LdapUserDetailsImpl) authentication.getPrincipal();

        log.info("authentication: " + authentication);
        log.info("principal: " + principal);

        return "Spring Security + Spring LDAP Authentication Configuration Example";
    }

    @GetMapping("/managers")
    public String managers(){

        List<User> list  = ldapTemplate.search(LdapQueryBuilder.query().attributes("cn", "uid").where("cn").is("Matus Seidl"), new MyMapper());

//        List<User> list  = ldapTemplate.search(LdapQueryBuilder.query().attributes("cn", "uid").where("cn").is("Matus Seidl"), new AttributesMapper<User>() {
//            @Override
//            public User mapFromAttributes(Attributes attributes) throws NamingException {
//                User user = new User();
//                user.setName(attributes.get("cn")+"");
//                user.setNickName(attributes.get("uid")+"");
//
//                return user;
//            }
//        });



//       List<String> list =  ldapTemplate.search(LdapQueryBuilder.query().attributes("cn","uid").where("cn").is("Matus Seidl"), new AttributesMapper<String>() {
//            @Override
//            public String mapFromAttributes(Attributes attributes) throws NamingException {
//                StringBuilder builder = new StringBuilder();
//
//                builder.append("CN: " + attributes.get("cn")+ " "+ attributes.get("uid"));
//
//                return builder.toString();
//            }
//        });


        return "Hello managers : " + list.get(0).getName() + " "+ list.get(0).getNickName() ;
    }

    @GetMapping("/employees")
    public String employees(){
        return "Hello employees";
    }
}
