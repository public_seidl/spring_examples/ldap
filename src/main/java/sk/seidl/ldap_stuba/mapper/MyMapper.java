package sk.seidl.ldap_stuba.mapper;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.NameClassPairCallbackHandler;
import sk.seidl.ldap_stuba.models.User;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

public class MyMapper implements AttributesMapper<User> {
    @Override
    public User mapFromAttributes(Attributes attributes) throws NamingException {
        User user = new User();
                user.setName(attributes.get("cn")+"");
                user.setNickName(attributes.get("uid")+"");
        return user;
    }
}
