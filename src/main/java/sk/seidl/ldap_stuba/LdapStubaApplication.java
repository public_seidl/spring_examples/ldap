package sk.seidl.ldap_stuba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class LdapStubaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LdapStubaApplication.class, args);
	}
}
